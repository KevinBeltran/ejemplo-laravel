<?php

namespace App\Http\Controllers;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
 
    public function  index(){

	$usuarios = User::all();
	$title = 'Listao de usuarios';

	return view('usuarios', compact('usuarios', 'title'));

    }
}
