@extends ('plantilla')

@section('title', "Usuarios");
@section('content')
<div class=" p-3 justify-content-center">
	
<div class="col-md-8">
	<h1>{{$title}} </h1>
</div>
<div class="col-md-8">
	
	<table class="table">
  <thead style="background-color: rgb(0,0,0); 255);">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Lugar de nacimiento</th>
      <th scope="col">profesion</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($usuarios as $user)
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->lugar_de_nacimiento}}</td>
      <td>{{$user->profesion}}</td>
     @endforeach
    </tr>

  </tbody>
</table>
</div>
</div>



</body>
@endsection