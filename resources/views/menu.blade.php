<nav class="mb-1 navbar navbar-expand-lg navbar-dark bg-info">

  <a class="navbar-brand" href='#'>PRÁCTICA</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
  aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
  </button>


  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link" href='/usuarios'>Inicio
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Operaciones</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Extras</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbar-2" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false" href="#">Opciones
        </a>
        <div class="dropdown-menu dropdown-default" aria-labelledby="navbar-2">
          <a class="dropdown-item" href='/usuarios/nuevo'>Registrar Usuarios</a>
          <a class="dropdown-item" href='/usuarios/{id}'>Mostrar Usuario</a>
          <a class="dropdown-item" href="#">Eliminar Usuario</a>
          <a class="dropdown-item" href='/usuarios/{id}/edit'>Editar Usuario</a>
        </div>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto ">

      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbar-2" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false" href="#">
          <i class="fas fa-user"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbar-2">
          <a class="dropdown-item" href="#">Salir</a>
          <a class="dropdown-item" href="#">Cambiar Usuario</a>
          <a class="dropdown-item" href="#">Extras</a>
        </div>
      </li>
    </ul>
</nav>
  </div>

